library(tidyverse)
data_set <- read.csv("USvideos.csv")

#scatterplot
png("ScatterplotforViewsvsLikes.png")
plot(data_set$views, data_set$likes, main="Youtube Video's Views vs Likes - Country United States Of America",
     xlab="Number of Views",
     ylab="Number of Likes",
     pch=19,
     frame=T,
     cex = .25)

abline(lm(data_set$likes ~ data_set$views,data=data_set),col="blue")
legend("bottomright", inset=.02, legend=c("Linear model"),
       col=c("blue"), lty=1:4, cex=0.8)
dev.off()

#Histogram Likes
png("histogramforLikes.png")
histLikes <- hist(data_set$likes,10
                  ,main="Frequency Distribution for Likes",xlab="Number of Likes",ylab="Frequency",freq=F)

likemean <- mean(data_set$likes)
likesd <- sd(data_set$likes)
lines(seq(0,6e+06,by=1e+05), dnorm(seq(0,6e+06,by=1e+05),likemean,likesd),col="blue")
legend("topright", inset=.02, legend=c("Normal Distribution"),
       col=c("blue"), lty=1:2, cex=0.8)
dev.off()

#Histogram views
png("histogramforViews.png")
histViews <- hist(data_set$views,main="Frequency Distribution for Views",xlab="Number of Views",ylab="Frequency",freq=F)
viewsmean <- mean(data_set$views)
viewssd <- sd(data_set$views)
lines(seq(0,2.0e+08,by=1.0e+06), dnorm(seq(0,2.0e+08,by=1.0e+06),viewsmean,viewssd),col="blue")
legend("topright", inset=.02, legend=c("Normal Distribution"),
       col=c("blue"), lty=1:2, cex=0.8)

dev.off()


#Analysis using spearman's method
cor.test(data_set$views,data_set$likes,method = "spearman")
